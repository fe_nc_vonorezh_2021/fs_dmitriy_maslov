function getUserInfo() {
    let name = prompt('Введите имя пользователя:', '')
    name = name.charAt(0).toUpperCase() + name.slice(1)

    let age
    do {
        age = prompt('Введите корректный возраст:', '')
        console.log(!isNaN(age))
    } while (isNaN(age) || age <= 0);

    alert('Привет, ' + name + ', тебе уже ' + age + ' лет!')
}


