function arrays() {
    let arr = Array.from({length: Math.random() * 100}, () => Math.floor(Math.random() * 100));
    console.log('Сгенерированный массив: ' + arr)
    alert('Сгенерированный массив: ' + arr)

    const sort = direction => {
        for (let i = arr.length - 1; i > 0; i--) {
            for (let j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    let value = arr[j]
                    arr[j] = arr[j + 1]
                    arr[j + 1] = value
                }
            }
        }
        return direction === 'desc' ? 'Отсортированный по убыванию массив: ' + arr.reverse() : 'Отсортированный по возрастанию массив: ' + arr
    }
    let directionVal = prompt('Введите направление сортировки(asc-по возрастанию; desc-по убыванию):', '')
    console.log(sort(directionVal))
    alert(sort(directionVal))


    const sumOfSqrOfOddElem = array => {
        return array.filter(n => n % 2).map(i => n += i * i, n = 0).reverse()[0]
    }

    console.log('Сумма квадратов нечётных элементов массива: ' + sumOfSqrOfOddElem(arr))
    alert('Сумма квадратов нечётных элементов массива: ' + sumOfSqrOfOddElem(arr))
}