function game1to1000() {
    do {
        let generatedNumber = Math.floor(Math.random() * 999 + 1)
        console.log(generatedNumber)

        let userNumber
        let counter = 0
        do {
            counter++
            userNumber = prompt('Введите число:', '')
            if (isNaN(userNumber) || userNumber === null || userNumber === '') alert('Введите число!')
            else {
                if (generatedNumber > userNumber) alert('Искомое число больше!')
                else if (generatedNumber < userNumber) alert('Искомое число меньше!')
            }
        } while (userNumber != generatedNumber)
        ans = confirm('Вы угадали! Количество попыток: ' + counter + '. Начать заново?')
    } while (ans)
}