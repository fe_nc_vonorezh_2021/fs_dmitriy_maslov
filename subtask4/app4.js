const fizzBuzz = num => {
    let numArray=[]
    for (let i=1;i<=num;i++) {
        if (!(i%3) && !(i%5)) numArray.push('FizzBuzz')
        else if (!(i%3)) numArray.push('Fizz')
        else if (!(i%5)) numArray.push('Buzz')
        else numArray.push(i)
    }
    alert(numArray)
    return numArray
}

//console.log(fizzBuzz(100))